const express = require('express')
const app = express()
const port = 3000

var now = new Date()
app.get('/', (req, res) => res.send('This application is deployed using Terraform ECS and Docker ! This instance was created on ' + now)) 

app.listen(port, () => console.log(`App lisenting on ${port}!`))
