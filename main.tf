provider "aws" {
  region = "ap-northeast-2"
}

resource "aws_ecr_repository" "this" {
  name = "node_demo"
}

resource "aws_ecs_cluster" "this" {
  name = "node_demo"
}

resource "aws_ecs_task_definition" "service" {
  family = "node_demo"

  container_definitions = <<DEFINITION
  [
  {
    "name": "node_demo",
    "image": "${aws_ecr_repository.this.repository_url}",
    "essential": true,
    "portMappings": [
      {
        "containerPort": 3000,
        "hostPort": 3000
      }
    ],
    "memory": 512,
    "cpu": 256
  }
]
  DEFINITION

  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"

  cpu    = 256
  memory = 512

  execution_role_arn = aws_iam_role.ecs_task_execution_role.arn
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "ecs_task_execution_role"

  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_ecs_service" "this" {
  name = "node_demo"

  cluster         = aws_ecs_cluster.this.id
  task_definition = aws_ecs_task_definition.service.arn

  launch_type   = "FARGATE"
  desired_count = 2

  network_configuration {
    subnets          = [aws_default_subnet.a.id, aws_default_subnet.b.id, aws_default_subnet.c.id, aws_default_subnet.d.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.fargate_node_demo.arn

    container_name = aws_ecs_task_definition.service.family
    container_port = 3000
  }


}

resource "aws_default_vpc" "this" {

}

resource "aws_default_subnet" "a" {
  availability_zone = "ap-northeast-2a"
}

resource "aws_default_subnet" "b" {
  availability_zone = "ap-northeast-2b"
}

resource "aws_default_subnet" "c" {
  availability_zone = "ap-northeast-2c"
}

resource "aws_default_subnet" "d" {
  availability_zone = "ap-northeast-2d"

}

resource "aws_alb" "this" {
  # Error: only alphanumeric characters and hyphens allowed in "name": "node_demo_alb"
  name = "node-demo-alb"

  load_balancer_type = "application"

  subnets = [aws_default_subnet.a.id,
    aws_default_subnet.b.id,
    aws_default_subnet.c.id,
  aws_default_subnet.d.id]

  security_groups = [aws_security_group.load_balancer.id]
}

resource "aws_security_group" "load_balancer" {
  ingress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    from_port        = 80
    protocol         = "tcp"
    self             = false
    to_port          = 80
    ipv6_cidr_blocks = ["::/0"]
    self             = false
    prefix_list_ids  = []
    security_groups  = []
    description      = ""
  }]

  egress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    from_port        = 0
    self             = false
    protocol         = "-1"
    to_port          = 0
    ipv6_cidr_blocks = ["::/0"]
    self             = false
    prefix_list_ids  = []
    security_groups  = []
    description      = ""
  }]
}

resource "aws_lb_listener" "fargate_node_demo" {
  load_balancer_arn = aws_alb.this.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.fargate_node_demo.arn
  }
}
resource "aws_lb_target_group" "fargate_node_demo" {
  name        = "node-demo-target-group"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_default_vpc.this.id
  health_check {
    matcher = "200,301,302"
    path    = "/"
  }
}
output "ecr_repo_url" {
  value = aws_ecr_repository.this.repository_url
}

