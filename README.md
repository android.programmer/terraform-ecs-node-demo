# terraform-ecs-node-demo

A simple nodejs server deployment using Terraform, Docker and ECS.

### Naming convention
This project follows the following naming conventions:
- https://www.terraform-best-practices.com/naming
- https://google.github.io/styleguide/jsguide.html
### Setup
- [terraform ](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [docker](https://docs.docker.com/get-docker/)
- [have your AWS account configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-config)
- [nodejs](https://nodejs.org/en/download/)

Once you have installed nodejs, install Express
```
npm install express
```

### Deploy
```
terraform init
terraform apply
```